import dateFormat from 'date-fns/format'
import Butter, {
  BlogPost as ButterBlogPost,
  PostListMetaData as ButterPostListMetaData
} from 'buttercms'

export const butter = Butter('20eb076018dd230e1c475b87234f84ee23a22025')

export interface BlogTag {
  path: string
  name: string
}

export type HTML = string

export interface BlogPost {
  title: string
  slug: string
  created: string
  summary: string
  body: HTML
  tags: BlogTag[]
}

export interface PagingMetaData {
  itemsPerPage: number
  total: number
  size: number
  index?: number
  minimum: number
  maximum: number
  previous?: number
  next?: number
}

export interface BlogPostsPayload {
  posts: BlogPost[]
  page: PagingMetaData
}

const convertPost = (input: ButterBlogPost): BlogPost => {
  return {
    title: input.title,
    slug: input.slug,
    created: input.created,
    summary: input.summary,
    body: input.body,
    tags: input.tags.map(tag => {
      return { path: '/', name: tag.name }
    })
  }
}

const convertPagingMetaData = (
  input: ButterPostListMetaData,
  page: number,
  size: number
): PagingMetaData => {
  return {
    itemsPerPage: size,
    total: input.count,
    size,
    index: page,
    minimum: 1,
    maximum: Math.ceil(input.count / size),
    previous: input.previous_page,
    next: input.next_page
  }
}

export const getBlogPost = async (slug: string): Promise<BlogPost> => {
  const result = await butter.post.retrieve(slug)
  return convertPost(result.data.data)
}

export const getBlogPosts = async (
  page: number,
  size: number
): Promise<BlogPostsPayload> => {
  try {
    const result = await butter.post.list({
      page,
      page_size: size,
      exclude_body: true
    })
    const posts = result.data.data.map(convertPost)
    const pageMeta = convertPagingMetaData(result.data.meta, page, size)
    return {
      posts,
      page: pageMeta
    }
  } catch (e) {
    const result = await butter.post.list({
      page: 1,
      page_size: size,
      exclude_body: true
    })
    const pageMeta = convertPagingMetaData(result.data.meta, page, size)
    pageMeta.index = undefined
    return {
      posts: [],
      page: pageMeta
    }
  }
}

export const postPath = (post: BlogPost): string => {
  const timestamp = dateFormat(post.created, 'yyyy/MM/dd')
  return `/posts/${timestamp}/${post.slug}`
}

export interface TextPage {
  title: string
  slug: string
  created: string
  body: HTML
}

const convertTextPage = (input: any): TextPage => {
  return {
    title: input.title,
    slug: input.slug,
    created: input.created,
    body: input.text
  }
}

export const getPage = async (slug: string): Promise<TextPage> => {
  const result = await butter.page.retrieve<any>('*', slug)

  console.log(result.data.data.fields)
  return convertTextPage(result.data.data.fields)
}
