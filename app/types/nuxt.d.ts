import VueRouter, { Route } from 'vue-router'
import { Store } from 'vuex'

declare module 'vue/types/vue' {
  interface Vue {
    $store: Store<any>
    $router: VueRouter
    $route: Route
    $util: {
      title: (str: string) => string
    }
  }
}
