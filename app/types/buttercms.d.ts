declare module 'buttercms' {
  import { AxiosResponse } from 'axios'

  export interface Author {
    slug: string
    first_name: string
    last_name: string
    email: string
    bio: string
    title: string
    linkedin_url: string
    facebook_url: string
    pinterest_url: string
    instagram_url: string
    twitter_handle: string
    profile_image: string
  }

  export interface Category {
    slug: string
    name: string
  }

  export interface Tag {
    slug: string
    name: string
  }

  export interface BlogPost {
    slug: string
    url: string
    published: string
    created: string
    status: string
    title: string
    body: string
    summary: string
    seo_title: string
    meta_description: string
    author: Author
    categories: Category[]
    tags: Tag[]
    featured_image: string
  }

  /*
    post.list
   */

  export interface PostListOptions {
    page?: number
    page_size?: number
    exclude_body?: boolean
    author_slug?: string
    category_slug?: string
    tag_slug?: string
  }

  export interface PostListMetaData {
    count: number
    next_page: number
    previous_page: number
  }

  export interface PostListJson {
    data: BlogPost[]
    meta: PostListMetaData
  }

  /*
    post.retrieve
   */

  export interface PostRetrieveJson {
    data: BlogPost
    meta: {
      count: number
      next_page: number
      previous_page: number
    }
  }

  /*
    page.retrieve
   */

  export interface PageRetrieveOptions {
    preview?: 1
    locale?: string
  }

  export interface PageRetrieveJson<T> {
    data: {
      slug: string
      fields: T
    }
  }

  /*
    buttercms api
   */

  export interface Buttercms {
    post: {
      list: (options: PostListOptions) => Promise<AxiosResponse<PostListJson>>
      retrieve: (slug: string) => Promise<AxiosResponse<PostRetrieveJson>>
    }
    category: any
    tag: any
    author: any
    feed: any
    content: any
    page: {
      retrieve: <T>(
        pageType: string,
        pageSlug: string,
        options?: PageRetrieveOptions
      ) => Promise<AxiosResponse<PageRetrieveJson<T>>>
    }
  }

  interface ButtercmsJs {
    (apiKey: string, testMode?: boolean, timeout?: number): Buttercms
  }

  const ButtercmsJsInstance: ButtercmsJs

  export default ButtercmsJsInstance
}
