export default (_, inject) => {
  // `i18n` キーを挿入する
  // -> app.$i18n になる
  // -> Vue コンポーネント内では this.$i18n
  // -> Vuex ストアやアクション、ミューテーション内で this.$i18n
  // このようにしてミドルウェアやページの asyncData や fetch の中でプラグインを使うことができる

  inject('util', {
    title(str) {
      if (str) {
        return `${str} | 電池冷蔵庫`
      }

      return '電池冷蔵庫'
    }
  })
}
