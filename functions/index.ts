import * as Functions from 'firebase-functions'
import * as Express from 'express'
import { Nuxt } from 'nuxt'

// nuxt
const config = {
  dev: false,
  buildDir: 'ssr', // relative path from working dir
  build: {
    publicPath: '/assets/'
  }
}
const nuxt = new Nuxt(config)

// express
const app = Express()
app.use(async (request, response) => {
  response.set('Cache-Control', 'public, max-age=300, s-maxage=600')
  await nuxt.render(request, response)
})

// firebase functions
export const nuxtApp = Functions.runWith({
  timeoutSeconds: 10,
  memory: '256MB'
}).https.onRequest(app)

// for test
if (process.env.NODE_ENV === 'development') {
  app.listen(3000)
  console.log('listening http://localhost:3000')
}
