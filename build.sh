#!/usr/bin/env bash

# remove cached files
rm -rf .build .public

# build
yarn run build:app
yarn run build:function

# copy nuxt app
mkdir -p .build
cp -R .nuxt .build/ssr
cp package.json .build/
cp yarn.lock .build/
cp functions/index.js .build/

# copy assets
mkdir -p .public
cp -R .nuxt/dist/client .public/assets
