const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

module.exports = {
  srcDir: 'app',
  extensions: ['vue', 'js', 'ts', 'scss', 'css'],
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: '電池冷蔵庫',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'なんかすごいブログ' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ['@/plugins/buefy', '@/plugins/util'],

  /*
  ** Nuxt.js modules
  */
  modules: [
    ['@nuxtjs/google-analytics', {
      id: 'UA-129775157-1'
    }]
  ],

  /*
  ** Build configuration
  */
  build: {
    publicPath: '/assets/',

    extend(config) {
      // Add TypeScript loader
      config.module.rules.unshift({
        test: /((client|server)\.js)|(\.tsx?)$/,
        use: [
          { loader: 'cache-loader' },
          {
            loader: 'thread-loader',
            options: {
              workers: require('os').cpus().length - 1
            }
          },
          {
            loader: 'ts-loader',
            options: {
              happyPackMode: true,
              appendTsSuffixTo: [/\.vue$/]
            }
          }
        ],
        exclude: [/dist/, /\.nuxt/]
      })

      // Add TypeScript loader for vue files
      for (let rule of config.module.rules) {
        if (rule.loader === 'vue-loader') {
          rule.options.loaders = rule.options.loaders || {}
          rule.options.loaders.ts = {
            loader: 'ts-loader',
            options: {
              appendTsSuffixTo: [/\.vue$/]
            },
            exclude: [/dist/, /\.nuxt/]
          }
        }
      }

      // Add .ts extension in webpack resolve
      if (config.resolve.extensions.indexOf('.ts') === -1) {
        config.resolve.extensions.push('.ts')
      }

      // Add ts checker plugin, because thread-loader requires happyPackMode (which hides all errors)
      config.plugins.push(
        new ForkTsCheckerWebpackPlugin({
          tsconfig: './app/tsconfig.json',
          checkSyntacticErrors: true,
          vue: true
        })
      )
    }
  }
}
